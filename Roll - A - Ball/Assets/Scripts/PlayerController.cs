﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;
using System.Collections.Specialized;

public class PlayerController : MonoBehaviour
{
    public float speed = 0;
    public TextMeshProUGUI countText;
    public GameObject winTextObject;

    private Rigidbody rb;
    private int count;
    private float movementX;
    private float movementY;
    public float JumpForce;
    public float threshold;

    /*Interactions with jumpPad*/


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;

        SetCountText();
        winTextObject.SetActive(false);
    }

    void OnMove(InputValue movementValue)
    {
        Vector2 movementVector = movementValue.Get<Vector2>();

        movementX = movementVector.x;
        movementY = movementVector.y;
    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
        if (count >= 12)
        {
            winTextObject.SetActive(true);
        }
    }

    void FixedUpdate()
    {
        Vector3 movement = new Vector3(movementX, 0.0f, movementY);

        rb.AddForce(movement * speed);
        if (transform.position.y < threshold)
            transform.position = new Vector3(0, 3.5f, 0);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PickUp"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;

            SetCountText();
        }
        if (other.gameObject.CompareTag("JumpPad"))
        {
            Debug.Log("Collided");
            rb.AddForce(Vector3.up * JumpForce, ForceMode.Impulse);
        }
    }
}
/* https://answers.unity.com/questions/971938/respawn-from-falling-off-world.html#:~:text=Change%20the%200%2C%200%2C%200,your%20player%20will%20respawn%20at. */

/* https://stackoverflow.com/questions/49050646/creating-a-jump-pad-in-unity3d */

/* https://www.youtube.com/watch?v=rO19dA2jksk */